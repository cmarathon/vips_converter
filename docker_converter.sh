#!/bin/bash

# $1 = shared folder complete path like /home/ncampos/Desktop/riidpo_tools/vips_converter/shared_folder
# Shared folder is where the ndpi files should be
# Maybe, this command should be ran as SUDO
# example: ./docker_converter.sh /home/ncampos/Desktop/riidpo_tools/vips_converter/shared_folder

for i in $1/*.ndpi; do
	xbase=${i##*/}
	echo "Processing file $xbase"
	sudo docker run --rm -t -v $1:/slides vips_converter vips --vips-progress dzsave /slides/$xbase /slides/${xbase%.*};
	sudo docker run --rm -t -v $1:/slides vips_converter openslide-show-properties /slides/$xbase > $1/${xbase%.*}.kv

	if ls -la $1 | grep ${xbase%.*}.dzi; then
		sudo mv $1/$xbase $1/done/$xbase
		sudo mv $1/${xbase%.*}.dzi $1/dzi2send/${xbase%.*}.dzi
		sudo mv $1/${xbase%.*}_files $1/dzi2send/${xbase%.*}_files
		sudo mv $1/${xbase%.*}.kv $1/dzi2send/${xbase%.*}.kv
	else
		echo "!!!!!! File conversion unsuccessfull !!!!!!"
	fi
done
