#!/bin/bash

for i in $1/shared_folder/*.ndpi; do
	echo "Processing file ${i}"
	xbase=${i##*/}
	vips --vips-progress dzsave ${i} $1/shared_folder/${xbase%.*}
	openslide-show-properties ${i} > $1/shared_folder/${xbase%.*}.kv
	if ls -la ./shared_folder | grep ${xbase%.*}.dzi; then
		mv $1/shared_folder/$xbase $1/shared_folder/done/$xbase
		mv $1/shared_folder/${xbase%.*}_files $1/shared_folder/dzi2send/${xbase%.*}_files
		mv $1/shared_folder/${xbase%.*}.dzi $1/shared_folder/dzi2send/${xbase%.*}.dzi
		mv $1/shared_folder/${xbase%.*}.kv $1/shared_folder/dzi2send/${xbase%.*}.kv
	else
		echo "!!!!!! File conversion unseccessfull !!!!!!"
	fi
	
done
