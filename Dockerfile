
# Use an official Python runtime as a parent image
FROM python:2.7
#FROM ubuntu:17.10

# let the container know that there is no tty
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y \
	openslide-tools \
	python-openslide 

RUN pip install pyvips

RUN apt-get update && apt-get install -y \
	libvips \
	libvips-dev \
	libvips-tools 
