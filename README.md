Docker container with VIPS converter.

ndpi files placed inside "shared_folder"

Command line run example:
$ cd /home/ncampos/Desktop/riidpo_tools/vips_converter

$ sudo ./serial_converter.sh /home/ncampos/Desktop/riidpo_tools/vips_converter/shared_folder
(SUDO is needed to redirect meta data to files upon conversion)

OU

docker run --rm -v /home/ncampos/Desktop/riidpo_tools/vips_converter/shared_folder:/slides vips_converter vips --vips-progress dzsave /slides/11030.ndpi /slides/11030

INPUT: NDPI image files must be placed on shared folder
OUTPUT: DZI file, Image files and meta data from image is placed on "dzi2send" folder and ndpi image goes to "done" folder
